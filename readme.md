# Resumen de workflow
1. Crear una organización
2. Crear un proyecto dentro de la organización
3. Si el proyecto es privado agregar a todos los colaboradores.
4. Cada colaborador debe tener un Fork a su máquina.
5. Cada desarrollar debe clonar su fork a su  máquina.
6. El programador debe agregar los remotos correspondientes del proyecto original.
7. Se debe asegurar que todo esté correctamente sincronizado.
8. Se creará uan nueva rama para trabajar una funcionalidad.
9. Uan vez terminado el trabajo el desarrollador sube (push) esa rama a su fork.
10. En github le aparacerá la opción de crear el pull request.
11. Una vez creado el pull request el administrador del proyecto original lo revisará.
12. Se aprueba o se rechaza el pull request.
13. Cada dev debe revisar constantemente(fetch) si no hubo cambio en el proyecto original.
14. Se debe eliminar las ramas que ya fueron aprobados/rechazados.

### Esta es una prueba 

Hola como estas
